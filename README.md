# delightful fediverse clients [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of Fediverse clients for accessing server applications based on the ActivityPub protocol and related standards.

## Contents

- [Client apps](#client-apps)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Client apps

The applications in the table below are listed in alphabetic order. A :ghost: emoji in "License/language" column indicates that the code project has been inactive for over a year, or is officially abandoned.

| Client | Supported apps | Supported Platforms | License/language |
| :--- | :--- | :--- | :--- |
| [**AndStatus**](http://andstatus.org/) | ActivityPub C2S, Pump.io, Mastodon, GNU Social | Android | [`Apache-2.0, Kotlin`](https://github.com/andstatus/andstatus) |
| [**Brutaldon**](https://gitlab.com/brutaldon/brutaldon) | Mastodon, Pleroma | Web-based | [`AGPL-3.0, Python`](https://gitlab.com/brutaldon/brutaldon) |
| [**Cuckoo**](https://github.com/NanaMorse/Cuckoo.Plus) | Mastodon | Web-based | :ghost: [`MIT, Vue/Typescript`](https://github.com/NanaMorse/Cuckoo.Plus) |
| [**Fedilab**](https://fedilab.app/) | Mastodon, Pleroma, Peertube, GNU Social, Friendica, Pixelfed | Android | [`GPL-3.0, Java`](https://framagit.org/tom79/fedilab) |
| [**Fedilab Lite**](https://codeberg.org/tom79/Fedilab_Lite) | Mastodon, Pleroma, Peertube, GNU Social, Friendica, Pixelfed | Android | [`GPL-3.0, Java`](https://codeberg.org/tom79/Fedilab_Lite) |
| [**Gomphotherium**](https://マリウス.com/gomphotherium-a-command-line-mastodon-client/) | Mastodon | Linux-CLI | [`GPL-3.0, Go`](https://github.com/mrusme/gomphotherium) |
| [**Halcyon**](https://www.halcyon.social/) | Mastodon, Pleroma | Web-based | [`AGPL-3.0, PHP`](https://notabug.org/halcyon-suite/halcyon) |
| [**Husky**](https://git.mentality.rip/FWGS/Husky) | Mastodon | Android | [`GPL-3.0, Kotlin`](https://git.mentality.rip/FWGS/Husky) |
| [**Jerboa**](https://github.com/dessalines/jerboa) | Lemmy | Android | [`AGPL-3.0, Kotlin`](https://github.com/dessalines/jerboa) |
| [**Lemmur**](https://github.com/LemmurOrg/lemmur) | Lemmy | Android, Linux, Windows | [`GPL-2.0, Dart`](https://github.com/LemmurOrg/lemmur) |
| [**mastodon.el**](https://codeberg.org/martianh/mastodon.el) | Mastodon | Emacs | [`GPL-3.0, Emacs Lisp`](https://codeberg.org/martianh/mastodon.el) |
| [**Metatext**](https://metabolist.org/) | Mastodon | Android | [`GPL-3.0, Swift`](https://github.com/metabolist/metatext) |
| [**Mousetodon**](https://github.com/cerisara/mousetodon) | Mastodon | Android | [`AGPL-3.0, Java`](https://github.com/cerisara/mousetodon) |
| [**Nomad**](https://framagit.org/disroot/AndHub) | Hubzilla | Android | :ghost: [`GPLv3, Java`](https://framagit.org/disroot/AndHub) |
| [**Otter**](https://github.com/apognu/otter) | Funkwhale | Android | [`MIT, Kotlin`](https://github.com/apognu/otter) |
| [**P2Play**](https://personaljournal.ca/p2play) | PeerTube | Android | [`GPL-3.0, Kotlin`](https://gitlab.com/agosto182/p2play) |
| [**Pinafore**](https://pinafore.social/) | Mastodon | Web-based | [`AGPL-3.0, Javascript`](https://github.com/nolanlawson/pinafore) |
| [**PixelDroid**](https://gitlab.shinice.net/pixeldroid/PixelDroid) | Pixelfed | Android | [`GPL-3.0, Kotlin`](https://gitlab.shinice.net/pixeldroid/PixelDroid) |
| [**Resin**](https://github.com/natjms/resin) | Pixelfed | Android, iOS | [`GPL-3.0, Javascript`](https://github.com/natjms/resin) |
| [**Thorium**](https://github.com/sschueller/peertube-android) | PeerTube | Android | [`AGPL-3.0, Kotlin`](https://github.com/sschueller/peertube-android) |
| [**Toot**](https://github.com/ihabunek/toot) | Mastodon | Linux-CLI | [`GPL-3.0, Python`](https://github.com/ihabunek/toot) |
| [**Tootle**](https://github.com/bleakgrey/tootle) | Mastodon | Linux | [`GPL-3.0, Vala`](https://github.com/bleakgrey/tootle) |
| [**TubeLab**](https://framagit.org/tom79/fedilab-tube) | PeerTube | Android | [`GPL-3.0, Java`](https://framagit.org/tom79/fedilab-tube) |
| [**Twidere**](https://twidere.com/) | Mastodon | Android | [`GPL-3.0, Kotlin`](https://github.com/TwidereProject/Twidere-Android) |
| [**Twitlatte**](https://github.com/moko256/twitlatte) | Mastodon | Android | [`Apache-2.0, Kotlin`](https://github.com/moko256/twitlatte) |
| [**Tusky**](https://tusky.app/) | Mastodon | Android | [`GPL-3.0, Kotlin`](https://github.com/tuskyapp/Tusky) |
| [**Whalebird**](https://whalebird.social/en/desktop/contents) | Mastodon, Pleroma, Misskey | Linux, MacOS, Windows | [`MIT, Vue/Typescript`](https://github.com/h3poteto/whalebird-desktop) |

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/fediverse/delightful-fediverse-clients/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)
- [`@lostinlight`](https://codeberg.org/lostinlight)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)
